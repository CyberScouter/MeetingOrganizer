package s0564513.meetingorganizer.model.memento;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import s0564513.meetingorganizer.model.speaker.Speaker;
import s0564513.meetingorganizer.model.speaker.SpeakerImpl;
import s0564513.meetingorganizer.model.speaker.SpeakerList;
import s0564513.meetingorganizer.model.speaker.SpeakerListImpl;

/**
 *
 */
abstract class StringSpeakerListMemento implements SpeakerListMemento {

    private String json;

    /**
     *
     * @param speakerList
     * @return
     */
    protected String speakerListToString(List<Speaker> speakerList) {
        if (speakerList != null) json = new Gson().toJson(speakerList);
        System.out.println(json);
        return json;
    }

    /**
     *
     * @param json
     * @return
     */
    protected List<Speaker> stringToSpeakerList(String json) {
        Type type = new TypeToken<ArrayList<SpeakerImpl>>(){}.getType();
        return new Gson().fromJson(json, type);
    }

    /**
     *
     * @param speakerList
     */
    @Override
    public void saveSpeakerList(List<Speaker> speakerList) {
        this.json = this.speakerListToString(speakerList);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Speaker> getSpeakerList() {
        return this.stringToSpeakerList(this.json);
    }
}
