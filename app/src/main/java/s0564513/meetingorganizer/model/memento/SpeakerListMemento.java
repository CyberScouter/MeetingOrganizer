package s0564513.meetingorganizer.model.memento;

import java.util.List;

import s0564513.meetingorganizer.model.speaker.Speaker;

/**
 *
 */
public interface SpeakerListMemento {
    /**
     *
     * @param speakerList
     */
    void saveSpeakerList(List<Speaker> speakerList);

    /**
     *
     * @return
     */
    List<Speaker> getSpeakerList();
}
