package s0564513.meetingorganizer.model.memento;

import android.app.Activity;
import android.content.Context;

import java.util.List;

import s0564513.meetingorganizer.model.speaker.Speaker;

/**
 *
 */
public class SharedPrefsSpeakerListMemento extends StringSpeakerListMemento implements SpeakerListMemento {

    private Context context;

    /**
     *
     * @param context
     */
    public SharedPrefsSpeakerListMemento(Context context) {
        this.context = context;
    }

    /**
     *
     * @param speakerList
     */
    @Override
    public void saveSpeakerList(List<Speaker> speakerList) {
        ((Activity) context).getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putString("speakerList", this.speakerListToString(speakerList))
                .apply();
    }

    /**
     *
     * @return
     */
    @Override
    public List<Speaker> getSpeakerList() {
        return this.stringToSpeakerList(((Activity) context).getPreferences(Context.MODE_PRIVATE)
                .getString("speakerList", ""));
    }
}
