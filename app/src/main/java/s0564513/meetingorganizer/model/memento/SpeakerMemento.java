package s0564513.meetingorganizer.model.memento;

import s0564513.meetingorganizer.model.speaker.Speaker;

/**
 *
 */
public interface SpeakerMemento {
    /**
     *
     * @param speaker
     */
    void saveSpeaker(Speaker speaker);

    /**
     *
     * @return
     */
    Speaker getSpeaker();
}
