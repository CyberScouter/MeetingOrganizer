package s0564513.meetingorganizer.model.memento;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import s0564513.meetingorganizer.model.speaker.Speaker;
import s0564513.meetingorganizer.model.speaker.SpeakerImpl;

/**
 *
 */
abstract class StringSpeakerMemento implements SpeakerMemento {

    private String json;

    /**
     *
     * @param speaker
     * @return
     */
    protected String speakerToString(Speaker speaker) {
        if (speaker != null) json = new Gson().toJson(speaker);
        return json;
    }

    /**
     *
     * @param json
     * @return
     */
    protected Speaker stringToSpeaker(String json) {
        Type type = new TypeToken<SpeakerImpl>(){}.getType();
        return new Gson().fromJson(json, type);
    }

    /**
     *
     * @param speaker
     */
    @Override
    public void saveSpeaker(Speaker speaker) {
        this.speakerToString(speaker);
    }

    /**
     *
     * @return
     */
    @Override
    public Speaker getSpeaker() {
        return this.stringToSpeaker(this.json);
    }
}
