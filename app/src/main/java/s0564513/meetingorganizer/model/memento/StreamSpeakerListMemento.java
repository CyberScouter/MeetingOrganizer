package s0564513.meetingorganizer.model.memento;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

import s0564513.meetingorganizer.model.speaker.Speaker;
import s0564513.meetingorganizer.model.speaker.SpeakerList;

/**
 *
 */
public class StreamSpeakerListMemento extends StringSpeakerListMemento implements SpeakerListMemento {

    private DataInputStream dis;
    private DataOutputStream dos;

    /**
     *
     * @param dis
     */
    public StreamSpeakerListMemento(DataInputStream dis) {
        this.dis = dis;
    }

    /**
     *
     * @param dos
     */
    public StreamSpeakerListMemento(DataOutputStream dos) {
        this.dos = dos;
    }

    /**
     *
     * @param speakerList
     */
    @Override
    public void saveSpeakerList(List<Speaker> speakerList) {
        String json = this.speakerListToString(speakerList);

        if (json.length() == 0 || this.dos == null) return;

        try {
            this.dos.writeUTF(json);
            this.dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return
     */
    @Override
    public List<Speaker> getSpeakerList() {
        if (this.dis == null) return null;

        String json = null;
        try {
            json = this.dis.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this.stringToSpeakerList(json);
    }
}
