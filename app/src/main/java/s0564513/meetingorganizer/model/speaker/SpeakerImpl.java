package s0564513.meetingorganizer.model.speaker;

import java.util.Random;

public class SpeakerImpl implements Speaker {

    private String name;
    private int id;

    public SpeakerImpl(String name) {
        this.name = name;
        this.id = new Random().nextInt();
    }

    public SpeakerImpl(Speaker speaker) {
        this.name = speaker.getName();
        this.id = new Random().nextInt();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getID() {
        return this.id;
    }
}
