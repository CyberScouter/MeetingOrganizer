package s0564513.meetingorganizer.model.speaker;

public interface Speaker {
    String getName();
    void setName(String name);
    int getID();
}
