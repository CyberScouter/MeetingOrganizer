package s0564513.meetingorganizer.model.speaker;

import java.util.ArrayList;
import java.util.List;

import s0564513.meetingorganizer.model.memento.SpeakerListMemento;

public class SpeakerListImpl implements SpeakerList {

    private List<Speaker> speakers;
    private static final int FIRST = 0;
    
    public SpeakerListImpl() {
        this.speakers = new ArrayList<>();
    }

    public SpeakerListImpl(List<Speaker> speakers) {
        this.speakers = new ArrayList<>(speakers);
    }

    @Override
    public List<Speaker> getSpeakers() {
        return new ArrayList<>(this.speakers);
    }

    @Override
    public boolean addSpeaker(Speaker speaker) {
        for (Speaker s:this.speakers)
            if (s.getName().equals(speaker.getName())) return false;
        this.speakers.add(speaker);
        return true;
    }

    @Override
    public boolean removeSpeaker(Speaker speaker) {
        if (this.getSpeakerByName(speaker.getName()) == null) return false;
        this.speakers.remove(this.getSpeakerByName(speaker.getName()));
        return true;
    }

    @Override
    public Speaker nextSpeaker() {

        if (!this.speakers.isEmpty()){
            return this.speakers.remove(FIRST);
        }
        else {
            return null;
        }
    }

    @Override
    public Speaker getSpeaker() {
        return this.speakers.get(FIRST);
    }

    @Override
    public int size() {
        return this.speakers.size();
    }

    @Override
    public void saveToMemento(SpeakerListMemento memento) {
        memento.saveSpeakerList(this.speakers);
    }

    @Override
    public void restoreFromMemento(SpeakerListMemento memento) {
        this.speakers = memento.getSpeakerList();
    }

    private Speaker getSpeakerByName(String name) {
        for (Speaker speaker:this.speakers) {
            if (speaker.getName().equals(name)) {
                return speaker;
            }
        }
        return null;
    }
}
