package s0564513.meetingorganizer.controller;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import s0564513.meetingorganizer.R;
import s0564513.meetingorganizer.model.speaker.Speaker;

public class SpeakersAdapter extends RecyclerView.Adapter<SpeakersAdapter.SpeakerViewHolder> {

    private List<Speaker> dataset;

    public static class SpeakerViewHolder extends RecyclerView.ViewHolder {
        public TextView textName;

        public SpeakerViewHolder(View view) {
            super(view);
            this.textName = view.findViewById(R.id.textName);
        }
    }

    public SpeakersAdapter(List<Speaker> dataset) {
        this.dataset = dataset;
    }

    @Override
    public SpeakersAdapter.SpeakerViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.speaker_list_row, parent, false);
        return new SpeakerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpeakerViewHolder holder, int position) {
        Speaker speaker = this.dataset.get(position);
        holder.textName.setText(speaker.getName());
    }

    @Override
    public int getItemCount() {
        return this.dataset.size();
    }
}
