package s0564513.meetingorganizer.view;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;

import s0564513.meetingorganizer.R;
import s0564513.meetingorganizer.view.fragments.FirstRunDialogFragment;
import s0564513.meetingorganizer.view.fragments.SettingsFragment;
import s0564513.meetingorganizer.view.fragments.SpeakersListFragment;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content_frame, new SpeakersListFragment());
        fragmentTransaction.commit();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {
                        int id = item.getItemId();
                        Fragment fragment = null;

                        switch (id) {
                            case R.id.nav_speakers_list:
                                fragment = new SpeakersListFragment();
                                break;
                            case R.id.nav_settings:
                                fragment = new SettingsFragment();
                                break;
                        }

                        if (fragment != null) {
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.content_frame, fragment);
                            //fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                            fragmentTransaction.commit();
                        }

                        drawerLayout.closeDrawer(GravityCompat.START);

                        return true;
                    }
                });

        checkFirstRun();
    }

    private void checkFirstRun() {
        boolean isFirstRun = getPreferences(Context.MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun) showDialog();
    }

    private void showDialog() {
        DialogFragment dialog = new FirstRunDialogFragment();
        dialog.show(getSupportFragmentManager(), "firstRunDialogFragment");
    }
}
