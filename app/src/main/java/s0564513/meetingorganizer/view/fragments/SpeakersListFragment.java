package s0564513.meetingorganizer.view.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import s0564513.meetingorganizer.R;
import s0564513.meetingorganizer.controller.SpeakersAdapter;
import s0564513.meetingorganizer.model.memento.SharedPrefsSpeakerListMemento;
import s0564513.meetingorganizer.model.memento.SharedPrefsSpeakerMemento;
import s0564513.meetingorganizer.model.memento.SpeakerListMemento;
import s0564513.meetingorganizer.model.memento.SpeakerMemento;
import s0564513.meetingorganizer.model.speaker.Speaker;
import s0564513.meetingorganizer.model.speaker.SpeakerImpl;
import s0564513.meetingorganizer.model.speaker.SpeakerList;
import s0564513.meetingorganizer.model.speaker.SpeakerListImpl;

public class SpeakersListFragment extends Fragment {

    private SpeakerList speakersList;
    private List<Speaker> nextSpeakersList;
    private Speaker speaker;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private View fragmentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SpeakerListMemento speakerListMemento = new SharedPrefsSpeakerListMemento(getActivity());
        if (speakerListMemento.getSpeakerList() != null) {
            this.speakersList = new SpeakerListImpl(speakerListMemento.getSpeakerList());
            this.nextSpeakersList = new ArrayList<>(this.speakersList.getSpeakers());
        } else {
            this.speakersList = new SpeakerListImpl();
            this.nextSpeakersList = new ArrayList<>();
        }

        String name = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString("pref_username", "");

        SpeakerMemento speakerMemento = new SharedPrefsSpeakerMemento(getActivity());
        if (speakerMemento.getSpeaker() != null) {
            this.speaker = new SpeakerImpl(speakerMemento.getSpeaker());
            this.speaker.setName(name);
        } else {
            this.speaker = new SpeakerImpl(name);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_speakers_list, container, false);

        recyclerView = fragmentView.findViewById(R.id.recyclerView);

        //recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(fragmentView.getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SpeakersAdapter(this.nextSpeakersList);
        recyclerView.setAdapter(adapter);

        ImageButton addButton = fragmentView.findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakersListFragment.this.addSpeaker();
            }
        });

        ImageButton removeButton = fragmentView.findViewById(R.id.removeButton);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakersListFragment.this.removeSpeaker();
            }
        });

        Button nextButton = fragmentView.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakersListFragment.this.nextSpeaker();
            }
        });

        return fragmentView;
    }

    @Override
    public void onPause() {
        super.onPause();

        SpeakerListMemento speakerListMemento = new SharedPrefsSpeakerListMemento(getActivity());
        this.speakersList.saveToMemento(speakerListMemento);

        //TODO: Speaker in Memento speichern
    }

    public void addSpeaker() {
        System.out.println(this.speakersList.addSpeaker(this.speaker));
        updateList();
    }

    public void removeSpeaker() {
        System.out.println(this.speakersList.removeSpeaker(this.speaker));
        updateList();
    }

    public void nextSpeaker() {
        TextView textView = fragmentView.findViewById(R.id.textCurrentSpeaker);
        if (speakersList.size()>0) textView.setText(this.speakersList.nextSpeaker().getName());
        else textView.setText(" ");
        updateList();
    }

    private void updateList() {
        nextSpeakersList.clear();
        nextSpeakersList.addAll(this.speakersList.getSpeakers());
        adapter.notifyDataSetChanged();
    }
}
