package s0564513.meetingorganizer.view.fragments;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import s0564513.meetingorganizer.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

    }
}
