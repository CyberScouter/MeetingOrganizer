package s0564513.meetingorganizer.view.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import java.util.regex.Pattern;

import s0564513.meetingorganizer.R;

public class FirstRunDialogFragment extends DialogFragment {

    private TextInputLayout textInputLayout;
    private TextInputEditText textInputEditText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_first_run_dialog, null);

        builder.setView(view)
                .setTitle("Enter your name")
                .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().getPreferences(Context.MODE_PRIVATE)
                                .edit()
                                .putBoolean("isFirstRun", false)
                                .apply();
                        PreferenceManager.getDefaultSharedPreferences(getActivity())
                                .edit()
                                .putString("pref_username", textInputEditText.getText().toString())
                                .apply();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getActivity().getPreferences(Context.MODE_PRIVATE)
                                .edit()
                                .putBoolean("isFirstRun", true)
                                .apply();

                        FirstRunDialogFragment.this.getDialog().cancel();
                    }
                });

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        textInputLayout = view.findViewById(R.id.textInputLayout);
        textInputEditText = view.findViewById(R.id.etName);

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!isValidName())
                    showError();
                else
                    hideError();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return dialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        getActivity().finish();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean isValidName() {
        return Pattern.matches("^([A-Z][a-z]*\\s)*[A-Z][a-z]*$", textInputEditText.getText().toString());
    }

    private void showError() {
        textInputLayout.setError("No valid name");
    }

    private void hideError() {
        textInputLayout.setError("");
    }
}
